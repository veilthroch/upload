const request = require('request-promise-native');

function stringifyReplacer(key, value) {
	if (typeof value === 'bigint') {
		return value.toString() + 'n';
	} else {
		return value;
	}
}

// TODO: ugh, let backend send this later
let REASONS = {
	403: "Outdated uploader, please update", 
	404: "Region not supported",
	500: "Internal error, please report",
}

module.exports = class VeilthrochUpload {
	constructor(mod) {
		console.log(mod.info.version);

		this.mod = mod;
		mod.game.initialize('inventory');

		mod.command.add('veil', (command, ...args) => {
			switch(command){
				case "upload": this.command_upload(...args); break;
			}
		});	
	}

	command_upload() {
		const data = {
			server: this.mod.serverId, 
			player: this.mod.game.me.playerId, 
			player_name: this.mod.game.me.name,
			items: this.mod.game.inventory.equipmentItems
		};

		let data_string = JSON.stringify(data, stringifyReplacer);

		request({
			uri: "https://www.veilthroch.com/gear/upload/", 
			method: "POST",
	        headers: {
				'Content-Type': 'application/json',
				'X-Uploader-Version': this.mod.info.version,
    		},
			body: data_string,
			resolveWithFullResponse: true,
		})
		.catch(result => {
			this.mod.command.message("Upload failed - " + REASONS[result.statusCode])
		})
		.then(result => {
			this.mod.command.message("Uploaded");
		});
	}
}
