Veilthroch uploader
==== 
Simple module for Tera Toolbox for uploading player's gear to [Veilthroch](https://www.veilthroch.com/)

### Usage

* `/8 veil upload` for uploading current gear